package com.kshrd.articlemanagementsystem.configurations;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

	// Register 2 users for testing:
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		
		auth.inMemoryAuthentication()
		.withUser("chhaya").password("{noop}KSHRD12345;;;;;").roles("USER")
		.and()
		.withUser("admin").password("{noop}admin123").roles("USER","ADMIN");
		
	}
	
}
